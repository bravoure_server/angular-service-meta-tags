(function () {

    'use strict';

    function metaTags (
        ngMeta,
        $rootScope,
        $sce,
        $location,
        PATH_CONFIG,
        $controller,
        API_Service,
        $stateParams,
        $filter,
        $translate
    ) {
        return {
            'updateMetaTags': updateMetaTags
        };

        function updateMetaTags ($scope) {

            // Extend controller
            angular.extend (this, $controller ('baseController', {
                $scope: $scope
            }));

            metaTags ($scope);

            $rootScope.$on ('$stateChangeSuccess', function () {
                metaTags ($scope);
            });

        }

        function metaTags ($scope) {

            function getValue(key) { return $scope.getValueAPi($scope.content, key); }
            function isNotEmpty(x) { return x != null && x !== ''; };

            var meta_title = $scope.getValueAPi ($scope.content, 'meta_title');
            var content = $scope.content.content || $scope.content;
            var meta_title_og = (content.og_title != null && content.og_title != '') ? content.og_title : meta_title;
            var meta_keywords = $scope.getValueAPi ($scope.content, 'meta_keywords');
            var meta_type = $scope.getValueAPi ($scope.content, 'metaType');
            var fpAppId = data.fb_app_id;
            var shareImage = data.share_image != '' && data.share_image != null ? data.share_image : 'share.png';
            var sitename = (typeof data.metaSitename != 'undefined') ? data.metaSitename : '';

            /*
                - option 1: value in content['meta_description']
                - option 2: value in content['meta_description__non_translatable']
                - fallback: value in locale-xx.json
            */
            var meta_description = '';
            if (isNotEmpty(getValue('meta_description'))) {
                meta_description = getValue('meta_description');
            } else if (isNotEmpty(getValue('meta_description__non_translatable'))) {
                meta_description = getValue('meta_description__non_translatable');
            } else if ($translate.instant('meta.description') !== 'meta.description') {
                meta_description = $translate.instant('meta.description');
            }

            var locale,
                ogImageWidth,
                ogImageHeight,
                meta_og_image;

            for (var i = 0; i < $scope.vm.APP_CONFIG.length; i++) {
                if ($scope.vm.APP_CONFIG[i].content.content_type == 'language') {

                    locale = ($scope.vm.APP_CONFIG[i].content.locale != undefined) ? $scope.vm.APP_CONFIG[i].content.locale : '';

                    // TODO[FE] - 02/06/2017 by Ismael - where this values come from
                    ogImageWidth = ($scope.vm.APP_CONFIG[i].content.width != undefined) ? $scope.vm.APP_CONFIG[i].content.width : '1200';

                    // TODO[FE] - 02/06/2017 by Ismael - where this values come from
                    ogImageHeight = ($scope.vm.APP_CONFIG[i].content.height != undefined) ? $scope.vm.APP_CONFIG[i].content.height : '630';

                    var scopeImage = ($stateParams.identifier == 'top.landing_pages') ? $scope.content : $scope.content.content;
                    var mainImage = $scope.getImage (scopeImage, 'main_image');
                    mainImage = (typeof mainImage.length == 'number') ? mainImage[0] : mainImage;
                    var metaImageFound = '';

                    // Checking in the alternateSizes of the main image if the image is already generated
                    if (typeof mainImage.alternate_sizes != 'undefined') {
                        for (var i = 0; i < mainImage.alternate_sizes.length; i++) {

                            if (mainImage.alternate_sizes[i].meta.width == ogImageWidth && mainImage.alternate_sizes[i].meta.height == ogImageHeight) {
                                metaImageFound = $scope.vm.PATH_CONFIG.locations[mainImage.alternate_sizes[i].storage] + mainImage.alternate_sizes[i].file;
                            }
                        }
                    }

                    if (metaImageFound != '') {
                        // Using the image found in the Alternate_sizes form the main_image
                        meta_og_image = metaImageFound;
                    } else {

                        var obj = {};

                        obj.id = mainImage.id;
                        obj.width = 1200;
                        obj.height = 630;
                        obj.source = mainImage.source;

                        var objContainer = [];
                        objContainer[0] = obj;

                        // Using the default image
                        meta_og_image = $location.$$protocol + '://' + $location.$$host + '/' + PATH_CONFIG.ASSETS + 'images/mobile/' + shareImage;

                        if (mainImage) {
                            API_Service (host + 'api/images/resize', {}, true).save ({}, objContainer,
                                function success (response) {

                                    if (typeof response[0] != 'undefined') {
                                        var responseStorage = (typeof response[0] != 'undefined') ? response[0].storage : 'storage';
                                        meta_og_image = $scope.vm.PATH_CONFIG.locations[responseStorage] + response[0].file;
                                        ngMeta.setTag ('ogImage', meta_og_image);
                                    }

                                },
                                function error (data) {
                                    console.log ('images not regenerated');
                                });

                        }
                    }
                }
            }

            ngMeta.setTitle ($sce.trustAsHtml (meta_title));
            ngMeta.setTag ('ogTitle', $sce.trustAsHtml (meta_title_og));
            ngMeta.setTag ('ogDescription', $sce.trustAsHtml (meta_description));
            ngMeta.setTag ('description', $sce.trustAsHtml (meta_description));
            ngMeta.setTag ('ogSitename', sitename);

            $ ('#favicon').attr ('href', '/' + $rootScope.favicon);

            if (meta_keywords) {
                ngMeta.setTag ('keywords', $sce.trustAsHtml (meta_keywords));
            }

            var isoDate = '';
            var author = '';
            var type = 'website';

            if (typeof content.handle != 'undefined' && content.handle == 'news') {

                type = 'article';

                // redefines the author
                if (typeof content.author[0].content.title != 'undefined') {
                    author = content.author[0].content.title;
                }

                // redefines the article:published_time meta tag
                if (typeof content.posted_at != 'undefined') {
                    isoDate = $filter ('date') ($scope.getDate ($scope.content.content.posted_at), "yyyy-MM-ddTHH:mm:ssZ");
                }
            }

            if (meta_type) {
                type = $sce.trustAsHtml (meta_type);
            }

            ngMeta.setTag ('ogType', type);

            ngMeta.setTag ('articleAuthor', author);
            ngMeta.setTag ('articlePublishedTime', isoDate);

            ngMeta.setTag ('ogLocale', locale);

            ngMeta.setTag ('ogUrl', $location.$$absUrl);
            ngMeta.setTag ('ogAppId', fpAppId);

            ngMeta.setTag ('ogImage', meta_og_image);
            ngMeta.setTag ('ogImageWidth', ogImageWidth);
            ngMeta.setTag ('ogImageHeight', ogImageHeight);

            // Sets the Prerender Status Code
            ngMeta.setTag ('prerender-status-code', $rootScope.preRenderStatus);
            ngMeta.setTag ('prerender-status-code', $rootScope.preRenderStatus);

        }
    }

    metaTags.$inject = [
        'ngMeta',
        '$rootScope',
        '$sce',
        '$location',
        'PATH_CONFIG',
        '$controller',
        'API_Service',
        '$stateParams',
        '$filter',
        '$translate'
    ];

    angular
        .module ('bravoureAngularApp')
        .service ('metaTags', metaTags);

}) ();
