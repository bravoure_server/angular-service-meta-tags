## Angular Service Meta Tags Sizes / Bravoure component

This Component is used to print the correct meta tags on each page.

### **Versions:**

## [1.0.5] - 2018-04-03
### Fixed
- Fixed Error in Meta image when the API wasn't not sending it as array.

## [1.0.4] - 2018-02-22
### Fixed
- Fix meta_description being the translate key value instead of an empty string

## [1.0.3] - 2018-02-15
### Fixed
- Fix missing $translate dependency

## [1.0.2] - 2018-02-15
### Added
- Meta description falls back to "meta.description" value in locale-xx.json file

## [1.0.1] - 2018-01-16
### Added
- Share image could come from the parameters.js file

## [1.0.0]
### Added
- Initial Stable version

---------------------
